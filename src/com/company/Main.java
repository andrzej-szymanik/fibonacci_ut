package com.company;

public class Main {
    public static int fibonacci(int n) {
        if(n < 0){
            throw new RuntimeException("Invalid value provided");
        }
        if (n <= 1) {
            return n;
        } else {
            return fibonacci(n-2)+fibonacci(n-1);
        }
    }
}
