package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FibonacciTest {

    @Test
    public void fibonacciForZeroShouldEqualZero(){
        Assertions.assertEquals(Main.fibonacci(0), 0);
    }
    @Test
    public void fibonacciForOneShouldEqualOne(){
        Assertions.assertEquals(Main.fibonacci(1), 1);
    }
    @Test
    public void fibonacciForTenShouldEqual55(){
        Assertions.assertEquals(Main.fibonacci(10), 55);
    }
    @Test
    public void fibonacciShouldBeSumOfTwoPreviousValues(){
        int sumOfPrevious = Main.fibonacci(4) + Main.fibonacci(5);
        Assertions.assertEquals(Main.fibonacci(6), sumOfPrevious);
    }
    @Test
    public void programShouldThrowExceptionWhenValueIsLessThanZero(){
        Assertions.assertThrows(RuntimeException.class, () -> Main.fibonacci(-1));
    }

}
